<?php

namespace App\Services\Upload;

use App\Http\Requests\UploadRequest;
use App\Models\Upload;
use App\Services\Xml\GetFileStorage;
use Illuminate\Http\Response;

/**
 * Class StorageService
 * @package App\Services\Upload
 */
class SaveStorageService
{
    private Upload $uploadModel;

    /**
     * SaveStorageService constructor.
     * @param Upload $uploadModel
     */
    public function __construct(Upload $uploadModel)
    {
        $this->uploadModel = $uploadModel;
    }

    /**
     * @param UploadRequest $request
     * @return Upload
     */
    public function apply(UploadRequest $request): Upload
    {
            try {
                if ( $xml = $request->file('xml', false) ) {
                    $name = $xml->getClientOriginalName();
                    $xml->move('xmls', $name);
                    /** check xml valid */
                    $xmlFile = (new GetFileStorage($name))->convert();

                    if (null === $xmlFile) {
                        throw new \DomainException('This xml is invalid.');
                    }

                    $this->uploadModel->path = $name;
                    $this->uploadModel->saveOrFail();
                }
            } catch (\Exception $exception) {
                $message = $exception instanceof \DomainException
                    ? $exception->getMessage()
                    : 'It was not possible to register the xml process.';
                throw new \DomainException($message, Response::HTTP_BAD_REQUEST);
            }

            return $this->uploadModel;
    }
}
