<?php

namespace App\Services\XmlProcess;

use App\Http\Requests\XmlProcessRequest;
use App\Models\Upload;
use App\Models\XmlProcess;
use App\Services\Xml\GetFileStorage;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

/**
 * Class SaveService
 * @package App\Services\XmlProcess
 */
class SaveService
{
    /**
     * @var XmlProcess
     */
    private XmlProcess $xmlProcessModel;

    /**
     * @var Upload
     */
    private Upload $uploadModel;

    /**
     * SaveService constructor.
     * @param XmlProcess $xmlProcessModel
     * @param Upload $uploadModel
     */
    public function __construct(XmlProcess $xmlProcessModel, Upload $uploadModel)
    {
        $this->xmlProcessModel = $xmlProcessModel;
        $this->uploadModel = $uploadModel;
    }

    /**
     * @param XmlProcessRequest $request
     * @return bool
     */
    public function apply(XmlProcessRequest $request): bool
    {
        try {
            $xmlData = json_decode($request->get('xmls', ''));
            $xmlCollection = new \ArrayObject($xmlData, \ArrayObject::ARRAY_AS_PROPS);

            DB::beginTransaction();
            array_map(function ($xml) {
                $uploadDataBase = $this->uploadModel
                    ->where('id', $xml->id ?? 0)
                    ->where('processed', false)
                    ->firstOrFail();
                $uploadDataBase->processed = true;
                $getFileStorage = new GetFileStorage($uploadDataBase->path);
                $xmlFile = $getFileStorage->convert();

                if (null === $xmlFile) {
                    throw new \DomainException('This xml is invalid.');
                }

                $jsonConvert = json_encode($xmlFile);
                $this->xmlProcessModel->json_convert = $jsonConvert;
                $this->xmlProcessModel->upload_id = $uploadDataBase->id;
                $this->xmlProcessModel->saveOrFail();
                $uploadDataBase->saveOrFail();
                $getFileStorage->delete();
            }, $xmlCollection->getIterator()->getArrayCopy());
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            $message = $exception instanceof \DomainException
                ? $exception->getMessage()
                : 'It was not possible to register the xml process.';
            throw new \DomainException($message, Response::HTTP_BAD_REQUEST);
        }

        return true;
    }
}
