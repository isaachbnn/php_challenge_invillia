<?php

namespace App\Services\Xml;

/**
 * Class GetFileStorage
 * @package App\Services\Xml
 */
final class GetFileStorage
{
    /**
     * @var string
     */
    private string $xmlName;

    /**
     * GetFileStorage constructor.
     * @param string $xmlName
     */
    public function __construct(string $xmlName)
    {
        $this->xmlName = public_path("xmls/{$xmlName}");
    }

    /**
     * @return \SimpleXMLElement|null
     */
    public function convert(): ?\SimpleXMLElement
    {
        try {
            $simpleXMLElement = simplexml_load_string(file_get_contents($this->xmlName));
            return $simpleXMLElement ?? null;
        } catch (\Exception $exception) {
            $this->delete();
        }

        return null;
    }

    /**
     * @return bool
     */
    public function delete():bool
    {
        return unlink($this->xmlName);
    }
}
