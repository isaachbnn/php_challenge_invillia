<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadRequest;
use App\Models\Upload;
use App\Services\Upload\SaveStorageService;
use Illuminate\Http\Request;

/**
 * Class UploadController
 * @package App\Http\Controllers
 */
class UploadController extends Controller
{
    /**
     * @var Upload
     */
    private Upload $model;

    /**
     * @var SaveStorageService
     */
    private SaveStorageService $saveStorageService;

    /**
     * UploadController constructor.
     * @param Upload $model
     * @param SaveStorageService $saveStorageService
     */
    public function __construct(Upload $model, SaveStorageService $saveStorageService)
    {
        $this->model = $model;
        $this->saveStorageService = $saveStorageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $limit = $data['limit'] ?? 10;
        $order = $data['order']['dir'] ?? null;

        if ($order !== null) {
            $order = explode(',', $order);
        }

        $order[0] = $order[0] ?? 'id';
        $order[1] = $order[1] ?? 'asc';
        $where = $data['where'] ?? [];
        $like = null;

        if(!empty($data['search']) and is_array($data['search'])) {
            $like[0] = 'path';
            $like[1] = '%'.$data['search']['value'].'%';
        }

        $results = $this->model
            ->orderBy($order[0], $order[1])
            ->where(function ($query) use ($like) {
                if ($like) {
                    return $query->where($like[0], 'like', $like[1]);
                }

                return $query;
            })
            ->where($where)
            ->paginate($limit)
        ;

        return response()->json($results);
    }

    /**
     * @param UploadRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function store(UploadRequest $request)
    {
        $this->saveStorageService->apply($request);

        return redirect('/');
    }
}
