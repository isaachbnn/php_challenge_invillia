<?php

namespace App\Http\Controllers;

use App\Http\Requests\XmlProcessRequest;
use App\Models\XmlProcess;
use App\Services\XmlProcess\SaveService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class XmlProcessController
 * @package App\Http\Controllers
 * @OA\Info(
 *      version="0.0.1",
 *      title="PHP CHALLENGE",
 *      description="Class responsible for converting xml to json within the application.",
 *      @OA\Contact(
 *          email="isaachbnn@gmail.com"
 *      )
 * )
 */
class XmlProcessController extends Controller
{
    /**
     * @var XmlProcess
     */
    private XmlProcess $model;
    /**
     * @var SaveService
     */
    private SaveService $xmlProcessSave;

    /**
     * XmlProcessController constructor.
     * @param XmlProcess $model
     * @param SaveService $xmlProcessSave
     */
    public function __construct(XmlProcess $model, SaveService $xmlProcessSave)
    {
        $this->model = $model;
        $this->xmlProcessSave = $xmlProcessSave;
    }

    /**
     * @param XmlProcessRequest $request
     * @return JsonResponse
     */
    public function create(XmlProcessRequest $request): JsonResponse
    {
        $result = $this->xmlProcessSave->apply($request);

        return response()->json(['success' => $result], 201);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @OA\Get(
     *      path="/api/process",
     *      summary="Get information all from processed XML's.",
     *      description="Returns all xml's processed.",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      security={
     *         {
     *             "basic_auth": {"user:admin", "password:admin"}
     *         }
     *     },
     * )
     */
    public function index(Request $request): JsonResponse
    {
        $data = $request->all();
        $limit = $data['limit'] ?? 10;
        $order = $data['order'] ?? null;

        if ($order !== null) {
            $order = explode(',', $order);
        }

        $order[0] = $order[0] ?? 'id';
        $order[1] = $order[1] ?? 'asc';
        $where = $data['where'] ?? [];
        $like = null;

        if(!empty($data['like']) and is_array($data['like'])) {
            $like = $data['like'];
            $key = key(reset($like));
            $like[0] = $key;
            $like[1] = '%'.$like[$key].'%';
        }

        $results = $this->model
            ->orderBy($order[0], $order[1])
            ->where(function ($query) use ($like) {
                if ($like) {
                    return $query->where($like[0], 'like', $like[1]);
                }

                return $query;
            })
            ->where($where)
            ->paginate($limit);


        return response()->json($results);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @OA\Get(
     *      path="/api/process/{id}",
     *      summary="Get information from processed XML",
     *      description="Returns xmls processed.",
     *      @OA\Parameter(
     *          name="id",
     *          description="XML id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "basic_auth": {"user:admin", "password:admin"}
     *         }
     *     },
     * )
     */
    public function show(int $id): JsonResponse
    {
        /** @var Model $result */
        $result = $this->model
            ->where(function ($query) {
                return $query;
            })
            ->findOrFail($id);

        return response()->json($result);
    }
}
