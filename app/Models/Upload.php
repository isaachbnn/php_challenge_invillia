<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UploadModel
 * @package App\Models
 */
class Upload extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

}
