<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class XmlProcess
 * @package App
 */
class XmlProcess extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'upload_id', 'json_convert'];

    protected $appends = ['name', 'json'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function xml()
    {
        return $this->hasOne(Upload::class, 'id', 'upload_id');
    }

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        return $this->xml()->first()->path;
    }

    /**
     * @return mixed
     */
    public function getJsonAttribute()
    {
        return json_decode($this->json_convert);
    }

}
