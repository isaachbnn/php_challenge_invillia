### PHP CHALLENGE

#### The challenge

Your customer receives two XML models from his partner. This information must be
available for both web system and mobile app.

##### Must have:
 - An index page to upload the XML with a button to process it.
 - Rest APIs for the XML data, only the GET methods are needed.

#### Bonus points:
 - Drag and drop upload component.
 - Authentication method to the APIs.
 - Generated documentation for the APIs.
 - Unit/Functional tests.
 
#### Applied solution

Minha solução foi escrita utilizando o Laravel 7.x

#### Requirement

 - docker
 - docker-compose

##### Installation

Clone this repository

```bash
git clone git@gitlab.com:isaachbnn/php_challenge_invillia.git; cd php_challenge_invillia;
```

Copy the .env.example file to .env

``I left values ​​that make the system easy to start.``

```bash
cp .env.example .env
```

Now just go up our containers

```bash
docker-compose up --build -d
```

``Wait a few minutes.``

[Now just access our application](http://localhost:8000/)


#### Api Documentation

[http://localhost:8000/api/documentation](http://localhost:8000/api/documentation)

#### Basic Auth

 - user: admin
 - password: admin
