<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/uploads', ['uses'=>'UploadController@index', 'as'=>'uploads.index']);
Route::prefix('process')->group(function () {
    Route::post('/', ['uses'=>'XmlProcessController@create', 'as'=>'process.create']);
});


Route::middleware(['basicAuth'])->group(function () {
    Route::prefix('process')->group(function () {
        Route::get('/', 'XmlProcessController@index');
        Route::get('/{id}', 'XmlProcessController@show')->where('id', '[0-9]+');;
    });
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
