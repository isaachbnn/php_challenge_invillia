<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>PHP CHALLENGE</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('dropzone/dist/min/dropzone.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('datatables/datatables.min.css')}}">

        <!-- JS -->
        <script src="{{asset('jquery/jquery-3.4.1.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('dropzone/dist/min/dropzone.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('datatables/datatables.min.js')}}" type="text/javascript"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <form method="POST" action="/upload" enctype="multipart/form-data" class="dropzone">
                    @csrf
                    <div>
                        <label>Upload file xml</label>
                    </div>
                    <div>
{{--                        <input id="file" type="file" name="xml">--}}
{{--                        <button type="submit">Upload </button>--}}
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </form>
                <br/>
                <div>
                    <button id="process_xml">Process</button>
                </div>
                <br/>
                <table id="table_xml" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>Identifier</th>
                        <th>Name</th>
                        <th>Processed</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Script -->
        <script>
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone(".dropzone",{
                maxFilesize: 3,  // 3 mb
                acceptedFiles: ".xml",
            });
            myDropzone.on("sending", function(file, xhr, formData) {
                formData.append("_token", CSRF_TOKEN);
                formData.append("xml", file);
            }).on('complete', function () {
                location.reload();
            }).on('error', function (file, xhr) {
                alert(xhr.message);
            });

            $(document).ready(function() {
                const table = $('#table_xml').DataTable({
                    processing: true,
                    serverSide: true,
                    ordering: false,
                    paging: false,
                    lengthChange: false,
                    info: false,
                    ajax: {
                        url: "{{ route('uploads.index') }}",
                        type: 'GET',
                        dataSrc: 'data',
                        dataType: 'json'
                    },
                    columns: [
                        { data: 'id', name: 'Identifier', visible: true},
                        { data: 'path', name: 'Name', visible: true},
                        {
                            data: 'processed',
                            name: 'Processed',
                            visible: true,
                            render: function (data, type, row, meta) {
                                let url = "{{ route('process.create') }}" + '/' + row.id;
                                return !data ? 'Waiting for processing.' : '<a href="'+url+'" target="_blank">Visualizar</a>';
                            }
                        },
                    ],
                });

                const btn_process = document.querySelector('#process_xml');
                btn_process.addEventListener("click", function () {
                    const data = table.rows().data().filter(function notProcessed(data) {
                        return 0 === data.processed;
                    });

                    if (0 === data.length) {
                        return false;
                    }

                    const dataJson = JSON.stringify( data.toArray() );

                    $.ajax({
                        url: "{{ route('process.create') }}",
                        type: "POST",
                        dataType: 'json',
                        data: {xmls: dataJson},
                        success: function(response){
                            location.reload();
                        },
                        error: function (response) {
                            alert(response.message);
                        }
                    });
                });
            } );
        </script>
    </body>
</html>
